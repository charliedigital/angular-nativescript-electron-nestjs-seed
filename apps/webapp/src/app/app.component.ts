import { Component } from '@angular/core';

@Component({
  selector: 'angular-nativescript-electron-nestjs-seed-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angular-nativescript-electron-nestjs-seed';
}
