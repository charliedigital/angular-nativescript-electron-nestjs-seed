import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(
      by.css('angular-nativescript-electron-nestjs-seed-root h1')
    ).getText();
  }
}
